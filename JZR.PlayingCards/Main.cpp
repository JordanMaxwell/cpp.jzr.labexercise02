/*
* Lab Exercise #2 - Playing Cards
* Part 1: Jason Ryan 2020-02-09
* Part 2: Jordan Maxwell 2020-02-11
*/

#include <iostream>
#include <streambuf>

enum Rank
{
	// Rank value starts from 2
	Two = 2,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
	Ace, // Ace rank value is 14
};
enum Suit { Diamond, Club, Heart, Spade };

struct Card
{
	Rank Rank;
	Suit Suit;
};

/* Prints a card to console */
void PrintCard(Card card) {

	// Determine the suit label of the card
	std::string suit;
	switch (card.Suit) {
		case Suit::Diamond:
			suit = "Diamond";
			break;
		case Suit::Club:
			suit = "Club";
			break;
		case Suit::Heart:
			suit = "Heart";
			break;
		case Suit::Spade:
			suit = "Space";
			break;
	}

	// Determine the rank label of the card
	std::string rank;
	switch (card.Rank) {
		case Rank::Two:
			rank = "Two";
			break;
		case Rank::Three:
			rank = "Three";
			break;
		case Rank::Four:
			rank = "Four";
			break;
		case Rank::Five:
			rank = "Five";
			break;
		case Rank::Six:
			rank = "Six";
			break;
		case Rank::Seven:
			rank = "Seven";
			break;
		case Rank::Eight:
			rank = "Eight";
			break;
		case Rank::Nine:
			rank = "Nine";
			break;
		case Rank::Ten:
			rank = "Ten";
			break;
		case Rank::Jack:
			rank = "Jack";
			break;
		case Rank::Queen:
			rank = "Queen";
			break;
		case Rank::King: 
			rank = "King";
			break;
		case Rank::Ace:
			rank = "Ace";
			break;
	}

	// Print result
	std::cout << "The " << rank << " of " << suit << std::endl;
}

/*
Returns the higher of the two cards by comparing its rank
*/
Card HighCard(Card card1, Card card2) {
	if (card1.Rank > card2.Rank)
		return card1;
	return card2;
}

/*
Main entry point for the application
*/
int main()
{
	// Create two sample cards
	Card card1 = {
		Rank::Ace,
		Suit::Diamond
	};

	Card card2 = {
		Rank::Five,
		Suit::Heart
	};

	// Print higher of the two samples
	Card compared = HighCard(card1, card2);
	PrintCard(compared);

	return 0;
}
